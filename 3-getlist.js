function get_List(id) {

    return new Promise((resolve, reject) => {
        const key = 'c7db9cc0857854022051b6a31fa55037'
        const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'
        fetch(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`, {
            method: 'GET',
        })
            .then((data) => {
                resolve(data.json())
            })
            .catch((err) => {
                reject(err)
            })
    })
}

get_List('66545004d3a95d4b6dba8b63')
    .then((data) => {
        console.log(data)
    })
    .catch((err) => {
        console.log(err)
    })

module.exports = get_List

