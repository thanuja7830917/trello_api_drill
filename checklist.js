const key = 'c7db9cc0857854022051b6a31fa55037'

const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'

const card_id = '665503816fce131958a6b17d'

function Checklist(cardId) {
    return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log('Checklists:', data);
        return data;
    })
    .catch(err => console.error('Error fetching checklists:', err));
}

Checklist(card_id)
    .then(checklists => {
        checklists.forEach(checklist => {
            console.log(`Checklist ID: ${checklist.id}, Name: ${checklist.name}`);
        });
    });