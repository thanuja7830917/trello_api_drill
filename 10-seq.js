const key = 'c7db9cc0857854022051b6a31fa55037'

const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'

function items(checklistId) {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`;
    return fetch(url)
        .then(response => response.json())
        .catch(error => {
            console.error('Error fetching checkitems:', error);
            throw error;
        });
}

function incomplete(card_id, checkitemId) {
    const url = `https://api.trello.com/1/cards/${card_id}/checkItem/${checkitemId}?state=incomplete&key=${key}&token=${token}`;
    return fetch(url, {
        method: 'PUT'
    })
    .then(response => response.json())
    .catch(error => {
        console.error('Error updating checkitem to incomplete:', error);
        throw error;
    });
}
function update(checklist_id, card_id) {
    return items(checklist_id)
        .then(checkitems => {
            const updatePromises = checkitems.map(checkitem => {
                return () => incomplete(card_id, checkitem.id)
                    .then(() => setTimeout(()=>{},1000)); // Wait for 1 second
            });
            return updatePromises.reduce((chain, update) => {
                return chain.then(update);
            }, Promise.resolve());
        })
        .then(() => {
            console.log('All checkitems are marked as incomplete');
        })
        .catch(error => {
            console.error('Error updating checkitems to incomplete:', error);
        });
}

const checklist_id = '665503e31876d61d9d48dff5';
const card_id = '665503816fce131958a6b17d';
update(checklist_id, card_id);
