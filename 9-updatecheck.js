const key = 'c7db9cc0857854022051b6a31fa55037'

const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'

function check_items(checklistId) {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`;
    return fetch(url)
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

function check(cardId, checkitemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=complete&key=${key}&token=${token}`;
    return fetch(url, {
        method: 'PUT'
    })
        .then(response => response.json())
        .catch(error => {
            console.error(`${checkitemId}`, error);
        });
}


function update(checklistId, cardId) {
    return check_items(checklistId)
        .then(checkitems => {
            const updatePromises = checkitems.map(checkitem =>
                check(cardId, checkitem.id)
            );
            return Promise.all(updatePromises);
        })
        .then(() => {
            console.log('check items are updated');
        })
        .catch(error => {
            console.error(error);
        });
}

const checklistId = '665503e31876d61d9d48dff5';
const cardId = '665503816fce131958a6b17d';
update(checklistId, cardId);