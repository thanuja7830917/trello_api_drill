//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously
const key = 'c7db9cc0857854022051b6a31fa55037'

const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'

function Create_Board() {

    return new Promise((resolve, reject) => {

        const boardName = 'sample3'

        fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${key}&token=${token}`, {
            method: "POST",
        })
            .then(response => response.json())
            .then((board) => {
                const listpromises = [];

                const lists = ['list1', 'list2', 'list3']

                for (let i = 0; i < 3; i++) {
                    listpromises.push(
                        create_list(board.id, lists[i]).then((list) => create_card(list.id))
                    );
                }
                return Promise.all(listpromises);
            })
            .then((created) => {
                resolve(created);
            })
            .catch((error) => {
                reject(error);
            });

    });
}

function create_list(id, name) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=${key}&token=${token}`, {
            method: 'POST'
        })

            .then(response => {
                return response.json();
            })

            .then((data) => {
                resolve(data)
            })
    })
}

function create_card(list_id) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${list_id}&key=${key}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                return response.json();
            })
            .then(response => {
                resolve(response)
            })
            .catch(err => console.error(err));
    })

}

Create_Board().then((data) => {
    console.log(data)
}).catch((err)=>{
    console.log(err)
})

module.exports = Create_Board

