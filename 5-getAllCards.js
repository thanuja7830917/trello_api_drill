const get_List = require("./3-getlist");
const get_Card = require("./4-getCard");

function getAllCards(id) {
  
  return new Promise((resolve, reject) => {
    get_List(id).then((data) => {
      data.forEach(element => {
        get_Card(element.id).then((data) => {
          resolve(data)
        })
      });
    })
  })
}

getAllCards('66545004d3a95d4b6dba8b63')
  .then((data) => {
    console.log(data)
  })
  .catch((err)=>{
    console.log(err)
  })

