const Create_Board = require("./6-create_board");

const key = 'c7db9cc0857854022051b6a31fa55037'

const token = 'ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA'

function creating() {
    return new Promise((resolve, reject) => {
        Create_Board().then((data) => {
            for (let list of data) {
                deleteList(list.idList)
            }
        }).then(resolve('deleted'))
    })
}
function deleteList(listID) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listID}/closed?value=true&key=${key}&token=${token}`, {
            method: "PUT",
        })
            .then(response => response.json())
            .then((deletedCard) => {
                resolve(deletedCard);
            }).catch((error) => {
                reject(error);
            });
    });
}
creating().then((result) => { console.log(result) })
